package com.example.myappcurriculum.Controlador;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {
    int numoftabs;

    public PagerAdapter(@NonNull FragmentManager fm, int numoftabs) {
        super(fm, numoftabs);
        this.numoftabs = numoftabs;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Reconocimiento();
            case 1:
                return new Educacion();
            case 2:
                return new Contacto();
            case 3:
                return new Referencias();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numoftabs;
    }
}
